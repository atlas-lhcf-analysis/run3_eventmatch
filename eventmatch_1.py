import pandas as pd 
import glob
import os
import re

## set run
def SetUpRun():
    # skip run such as moving, emittance scan etc
    # There is not lb coressponding to 80614 
    skip_run = [80347, 80348, 80349, 80350, 80373, 80374, 80375, 80376, 80432, 80466, 80467,
                80468, 80369, 80469, 80470, 80521, 80531, 80532, 80533, 80534, 80535, 80612,
                80613, 80614, 80615, 80616, 80627, 80628, 80629, 80630, 80631, 80632, 80633,
                80634]
    not_good_run = [80433, 80522] #Not good run
    check_run = [80509] # Pyhsics (check)
    

    skip_run.extend(not_good_run)
    skip_run.extend(check_run)
    skip_run = sorted(skip_run)
    # arm2 skip run
    # 80465 is not in  /eos/experiment/lhcf/ReducedData/
    # 80465 is 'Physics' in run Table Op2022
    si_problem_run = [80397, 80465, 80506, 80507, 80549]
    only_arm1_run = [80398, 80399, 80400, 80508, 80550, 80551]
    fc_error_run = [80612]

    only_arm1_run.extend(si_problem_run)
    only_arm1_run.extend(fc_error_run)
    only_arm1_run = sorted(only_arm1_run)
    return skip_run, only_arm1_run

## Arm1 File Read
def ReadArm1File(filename, arm=0):
    names = ['Arm1RunNumber','Arm1EntryNumber','Arm1Time','Arm1Time_Usec','Arm1Bcid','Arm1Ecr','Arm1ATLASlevel1id','Arm1ATLAStrigerAccept']
    dtype = {'Arm1RunNumber':'Int64','Arm1EntryNumber':'Int64','Arm1Time':'Int64','Arm1Time_Usec':'Int64','Arm1Bcid':'Int64','Arm1Ecr':'Int64','Arm1ATLASlevel1id':'Int64','Arm1ATLAStrigerAccept':'Int64'}
    df = pd.read_csv(filename, names=names, header=None, dtype=dtype, sep=' ')
    if arm == 1:
        df['lhcf_ecr'] = df['Arm1Ecr'] % 256
        df['lhcf_l1id'] = df['Arm1ATLASlevel1id']
        df['lhcf_time'] = df['Arm1Time']
    n = df.shape[0]
    # drop non-common events 
    df = df[df['Arm1ATLAStrigerAccept'] == 1]
    n_sel = df.shape[0]
    print ('Arm1 Event Number = {} (non-common events = {})'.format(n, n-n_sel))
    return df    

## Arm2 File Read
def ReadArm2File(filename, arm=0):
    names = ['Arm2RunNumber','Arm2EntryNumber','Arm2Time','Arm2Time_Usec','Arm2Bcid','Arm2Ecr','Arm2ATLASlevel1id','Arm2ATLAStrigerAccept']
    dtype = {'Arm2RunNumber':'Int64','Arm2EntryNumber':'Int64','Arm2Time':'Int64','Arm2Time_Usec':'Int64','Arm2Bcid':'Int64','Arm2Ecr':'Int64','Arm2ATLASlevel1id':'Int64','Arm2ATLAStrigerAccept':'Int64'}
    df = pd.read_csv(filename, names=names, header=None, dtype=dtype, sep=' ')
    if arm == 2:
        df['lhcf_ecr'] = df['Arm2Ecr'] % 256
        df['lhcf_l1id'] = df['Arm2ATLASlevel1id']
        df['lhcf_time'] = df['Arm2Time']
    n = df.shape[0]
    # drop non-common events 
    df = df[df['Arm2ATLAStrigerAccept'] == 1]
    n_sel = df.shape[0]
    print ('Arm2 Event Number = {} (non-common events = {})'.format(n, n-n_sel))
    return df   

## ATLAS File Read
def ReadATLAS(path, lbid):
    all_files = glob.glob(path + "/lumiblock_{}_rootnumber_*.txt".format(lbid))
    dblist = []

    for filename in all_files:
        names = ['EntryNumber','FileNumber','RunNumber','Lumiblock','Bcid','TimeStamp','TimeStampNSOffset','Extendedlevel1id', 'HLT_noalg_L1LHCF']
        dtype = {'EntryNumber':'Int64','FileNumber':'Int64','RunNumber':'Int64','Lumiblock':'Int64','Bcid':'Int64','TimeStamp':'Int64','TimeStampNSOffset':'Int64','Extendedlevel1id':'Int64','HLT_noalg_L1LHCF':'Int64'}
        df = pd.read_csv(filename, names=names, header=None, sep=' ')
        dblist.append(df)
    
    df = pd.concat(dblist, axis=0, ignore_index=True)
    # Sort by time
    df = df.sort_values(by=['TimeStamp','TimeStampNSOffset'])
    # Split Extended l1id to ECR + l1id
    #df = df.astype('int64')
    df['ecr'] =  df['Extendedlevel1id'] // pow(2,24)
    df['l1id'] = df['Extendedlevel1id'] & 0xFFFFFF
    df = df.astype({'ecr': 'Int64', 'l1id':'Int64'})
    #print(df['EntryNumber'].nunique() == df.shape[0])
    return df

## Combine Arm1 and Arm2 data
def CombineArms(df_a1, df_a2):
    df_lhcf = pd.merge(df_a1, df_a2, how='outer', left_on=['Arm1Ecr','Arm1ATLASlevel1id'], right_on=['Arm2Ecr','Arm2ATLASlevel1id'])
    df_lhcf = df_lhcf.fillna(-1)  # replace N/A to -1
    n_a1 = df_a1.shape[0]
    n_a2 = df_a2.shape[0]
    n_lhcf = df_lhcf.shape[0]
    print("Marge Arm1 and Arm2: {} events (A1: {}, A2: {}) ".format(n_lhcf, n_a1, n_a2))

    # create common parameters(ecr, l1id) 
    df_lhcf['lhcf_ecr'] = [ a1 if a1 > 0 else a2 for a1, a2 in zip(df_lhcf['Arm1Ecr'], df_lhcf['Arm2Ecr']) ]
    df_lhcf['lhcf_ecr'] = df_lhcf['lhcf_ecr'] % 256
    df_lhcf['lhcf_l1id'] = [ a1 if a1 > 0 else a2 for a1, a2 in zip(df_lhcf['Arm1ATLASlevel1id'], df_lhcf['Arm2ATLASlevel1id']) ]
    df_lhcf['lhcf_time'] = [ a1 if a1 > 0 else a2 for a1, a2 in zip(df_lhcf['Arm1Time'], df_lhcf['Arm2Time']) ]
    df_lhcf = df_lhcf.astype({'lhcf_ecr': 'Int64', 'lhcf_l1id':'Int64','lhcf_time':'Int64'})

    df_lhcf = df_lhcf.sort_values(by=['lhcf_ecr','lhcf_l1id'])
    return df_lhcf

## ECR offset calculation 
def SearchECRoffset(df_lhcf, df_atlas, arm=0, reverse=False):
    if reverse :
        df_atlas = df_atlas[::-1]
    i = 0
    for index, row in df_atlas.iterrows():
        time = row['TimeStamp']
        l1id = row['l1id']
        if arm == 0:
            result = df_lhcf.query('lhcf_time == {} &  lhcf_l1id == {}'.format(time, l1id), engine='python')
        else:
            result = df_lhcf.query('Arm{}Time == {} &  Arm{}ATLASlevel1id == {}'.format(arm, time, arm, l1id), engine='python')
        
        if result.shape[0] == 1: 
            #print(result)
            result = result.reset_index()
            atlas_ecr = row['ecr']
            if arm == 0:
                lhcf_ecr = result['lhcf_ecr'].iat[0]
            else:
                lhcf_ecr = result['Arm{}Ecr'.format(arm)].iat[0] % 256
            ecr_offset = lhcf_ecr - atlas_ecr
            if ecr_offset<0:
                ecr_offset += 256
            return ecr_offset
        # for debug
        i = i + 1
        if i>0 and i%500==0:
            print ('Matching for ECR offset {}'.format(i))

    print('Could not find the matched event between Arm{} and ATLAS'.format(arm))
    return -100000

def CalculateECRoffset(df_lhcf, df_atlas, arm=0, option=''):

    offset_first = offset_last = -1
    if option != 'skipfirst':
        # check ECR with the first samples in ATLAS data
        offset_first = SearchECRoffset(df_lhcf, df_atlas, arm, reverse=False)
    if option != 'skiplast':
        # check ECR with the last samples in ATLAS data
        offset_last = SearchECRoffset(df_lhcf, df_atlas, arm, reverse=True)

    if option == 'skipfirst':
        offset_first = offset_last
    if option == 'skiplast':
        offset_last = offset_first

    if offset_first != offset_last:
        print('ECR offset was not consistent in the data. ({}, {})'.format(offset_first, offset_last))
        return -100000
    else :
        return offset_first
    
def AdjustECR(df_atlas, offset):
    df_atlas['ecr_mod'] = (df_atlas['ecr'] + offset)%256
    df_atlas = df_atlas.astype({'ecr_mod': 'Int64'})
    return df_atlas

# Prepare ATLAS data for multiple lumiBlock data
def PrepareATLASdata(path, lb_llist, lb_first, lb_last, df_lhcf, arm=0):

    print ('Opening ATLAS data ....')

    dflist = []
    for lb in lb_list:
        # Open ATLAS Data   
        df = ReadATLAS(path,lb)
        #print(df['EntryNumber'].nunique() == df.shape[0],df['EntryNumber'].nunique(),df.shape[0])
        # ECR Offset comparing with LHCf data
        option = ''
        if lb == lb_first: 
            option = 'skipfirst'
        if lb == lb_last:
            option = 'skiplast'

        ecr_offset = CalculateECRoffset(df_lhcf, df, arm=arm, option=option)
        df = AdjustECR(df, ecr_offset)
        dflist.append(df)

        # dump information 
        print (" LumiBlock {} : {:>10d} events, ECR offset = {}".format(lb, df.shape[0], ecr_offset))
        
    df = pd.concat(dflist, axis=0, ignore_index=True)
    print('Total  {} events'.format(df.shape[0]))
    #print(df[df['l1id'] == 56825])
    return df

def EventMatching(df_lhcf, df_atlas):
    # Matching with ATLAS
    df = pd.merge(df_lhcf, df_atlas, how='left', left_on=['lhcf_ecr','lhcf_l1id'], right_on=['ecr_mod','l1id'])
    df = df.fillna(-1)  # replace N/A to -1
    df = df.astype({'FileNumber':'Int64', 'EntryNumber':'Int64'})
    # Print the results 
    n_lhcf = df_lhcf.shape[0]
    n_atlas = df_atlas.shape[0]
    #df_matched = df.query('EntryNumber >= 0', engine='python')
    df_matched = df[df['EntryNumber'] != -1]
    df_matched = df_matched.sort_values(by=['FileNumber', 'EntryNumber'])
    n_match = df_matched.shape[0]
    
    #######
    print("Event Matching result:")
    print("  LHCf events     {:>10d}".format(n_lhcf))
    print("  ATLAS events    {:>10d}".format(n_atlas))
    print("  Matched events  {:>10d}".format(n_match))
    return df_matched, df

def DumpResult(df, filename_minimum, filename_full, arm=0):
    df.to_csv(filename_full, index = False, sep=' ')
    if arm == 0:
        columns = ['Arm1RunNumber','Arm1EntryNumber','Arm2RunNumber','Arm2EntryNumber','FileNumber','EntryNumber']
    else :
        columns = [f'Arm{arm}RunNumber',f'Arm{arm}EntryNumber','FileNumber','EntryNumber']
    df.to_csv(filename_minimum, index = False, sep=' ', columns=columns)

def NotMatchingEvents(df,filename_missing, arm=0):
    df = df[df['EntryNumber'] == -1]
    if arm == 0:
        df = df[(df['Arm1EntryNumber'] != -1)|(df['Arm2EntryNumber'] != -1)]
    else:
        df = df[df[f'Arm{arm}EntryNumber'] != -1]
        
    if df.shape[0] != 0:
        df.to_csv(filename_missing, index = False, sep= ' ' )
        print(f'missing events:{df.shape[0]}')
    else:
        print('Non missing event')
        
### Make Run LB Tabele ###       
def get_lbid(filename):
    match = re.match(r'lumiblock_(\d{4})_rootnumber_(\d{3})\.txt', filename)    
    if match:
        return int(match.group(1)), int(match.group(2))
    else:
        return None

def lbid_list(atlas_path):
    filenames = os.listdir(atlas_path)
    filenames = sorted(filenames)
    lb_list=[]
    for filename in filenames:
        lbid = get_lbid(filename)[0]
        lb_list.append(lbid)
    lb_list = list(set(lb_list))
    return lb_list

def LHCfTime(df_a1, df_a2):
    df = pd.merge(df_a1, df_a2, how='outer', left_on='Arm1RunNumber', right_on='Arm2RunNumber')    
    df = df.fillna(-1)
    start1 = df['Arm1StartingTime'].tolist()
    start2 = df['Arm2StartingTime'].tolist()
    end1 = df['Arm1EndTime'].tolist()
    end2 = df['Arm2EndTime'].tolist()
    run1 = df['Arm1RunNumber'].tolist()
    run2 = df['Arm2RunNumber'].tolist()
    
    start_time = [arm1 if arm2 == -1 else arm2 if arm1 == -1 else min(arm1, arm2) for arm1, arm2 in zip(start1, start2)]
    end_time = [max(arm1, arm2) for arm1, arm2 in zip(end1, end2)]
    run = [int(max(arm1, arm2)) for arm1, arm2 in zip(run1, run2)]
    df_lhcf = pd.DataFrame()
    df_lhcf['Start'] = start_time
    df_lhcf['End'] = end_time
    df_lhcf['run'] = run
    df_lhcf = df_lhcf.query('run<80627')#skip Fill8181
    return df_lhcf

def MakeRunlbLine(df_at,df_a1,df_a2):
    df = pd.DataFrame()
    df_sta = pd.DataFrame()
    df_end = pd.DataFrame()
    df_lhcf = LHCfTime(df_a1,df_a2) 
    df_at = df_at.set_index('LumiBlock')
    df_lhcf = df_lhcf.set_index('run')
    df = pd.concat([df_at,df_lhcf],axis=0)
    df_sta = df.sort_values(['Start'])
    df_end = df.sort_values(['End'], ascending=False)
    df_sta = df_sta.reset_index()
    df_end = df_end.reset_index()
    return df_end,df_sta

def CheckMissing(lb_list):
    num_min = min(lb_list)
    num_max = max(lb_list)
    com_list = list(range(num_min,num_max+1))
    missing_list = [num for num in com_list if num not in lb_list]
    if len(missing_list):
        #print(f'number of missing files:{len(missing_list)}\n{missing_list}')
        return len(missing_list)
    else:
        print('not missing')
        return len(missing_list)

def DelRun(runlb_dict):
    delete_run= []
    for run in runlb_dict.keys():
        num = len(runlb_dict[run])
        if num == 0:
            delete_run.append(run)
        else:
            missing_list_len = CheckMissing(runlb_dict[run])
            if missing_list_len != 0:
                delete_run.append(run)
    ##--- loop ---##
    for run in delete_run:
        del runlb_dict[run]
    return runlb_dict

def Addlb(runlb_dict):
    prev_run = None
    for run in runlb_dict.keys():
        if runlb_dict[run] != []:
            prev_values = runlb_dict[run]
            if prev_run is not None:
                runlb_dict[run].append(runlb_dict[prev_run][-1])
                runlb_dict[run] = sorted(runlb_dict[run])
                prev_run = run
            prev_run = run
    return runlb_dict
    
def MakeRunlbDict(lines):
    ##--- set initial parameter ---##
    runlb_dict = {}
    current_run = None
    current_values = []
    keys_to_delete = []

    ##--- make run and lb dict ---##
    for line in lines:
        if 10000 <= line <= 99999:#get RunNumber
            runlb_dict[current_run] = current_values.copy()

            if current_run not in runlb_dict:
                runlb_dict[current_run] = []

            current_run = line
            current_values = []#initialization

        else:#get lbid
            current_values.append(line)

    if current_run is not None:
        runlb_dict[current_run] = current_values.copy()
    
    runlb_dict = Addlb(runlb_dict) #Add lb
    #runlb_dict = DelRun(runlb_dict) #Deletion of run corresponding to Non-Contiguous lb
    runlb_dict.pop(None)
    return runlb_dict

def MakeRunlbTable(df_at,df_a1,df_a2):
    df_end,df_sta = MakeRunlbLine(df_at,df_a1,df_a2)
    sta_lines = df_sta['index'].tolist()
    
    end_lines = df_end['index'].tolist()
    return MakeRunlbDict(sta_lines)

##### Main Function #####
if __name__ == '__main__':
    #### set path ####
    arm1_path = './LHCfArm1_data/'
    arm2_path = './LHCfArm2_data/'    
    atlas_path = './ATLAS_data/'
    save_path = './eventmatch/'
    nomatch_path = './nomatch/'
    version = 1    
    ##################
    
    ### Make lb Run Table ###
    df_atlas_time = pd.read_csv('./tables/nsectimelist_atlas_ver2.txt',names = ['Start','End','LumiBlock'],header = 0, sep = ' ')
    df_arm1_time = pd.read_csv('./tables/usectimelist_arm1_ver1.txt',header = 0, sep = ' ')
    df_arm2_time = pd.read_csv('./tables/usectimelist_arm2_ver1.txt',header = 0, sep = ' ')
    run_lb_dict = MakeRunlbTable(df_atlas_time,df_arm1_time,df_arm2_time)
    print(run_lb_dict[80300])
    #########################

    os.makedirs(nomatch_path, exist_ok=True)
    os.makedirs(save_path, exist_ok=True)    
    
    ### set skip run ###
    skip_run, only_arm1_run = SetUpRun()
    
    for run in run_lb_dict:
        # lb_first and lb_last 
        if os.path.exists(save_path + f'/matchlist_lhcfrun{run:d}_{version}.txt'):
            print('table run{:d} already exists. Skipping...'.format(run))
            continue
        if run != 80300:
            #print('skip')
            continue
        elif run_lb_dict[run] == []:
            print(f'lb file corresponding to {run} do not exist. Skipping...')
            continue
    
        elif run in skip_run:
            print(f'This run{run} is Not Good run,  check run etc...')
            continue
        
        elif run in only_arm1_run:
            print('Only arm1')
            
            ##### Set first and last lb file in the run ######
            num = len(run_lb_dict[run])
            lb_list = run_lb_dict[run]
            lb_first = run_lb_dict[run][0]
            lb_last = run_lb_dict[run][num-1]
            print(f'  RunNumber{run} correspond lb{lb_first} to lb{lb_last}  ')
            
            ##### Read arm1 data and ATLAS data#####  
            df_lhcf = ReadArm1File(arm1_path+f'LHCfArm1Run{run}.txt', arm=1)
            df_atlas = PrepareATLASdata(atlas_path, lb_list, lb_first, lb_last, df_lhcf, arm=0)

            ##### Event Matching ##### 
            print('Event Matching start--- ')
            df_matched, df_nomatched = EventMatching(df_lhcf, df_atlas)

            ##### Dump the matched results and the not matched result to CSV files #####
            print('\nDumping results to files...')
            DumpResult(df_matched, save_path+f'matchlist_lhcfrun{run}_{version}.txt', save_path+f'fulllist_lhcfrun{run}_{version}.txt', arm=1)
            print('\nNot matching event search...')
            NotMatchingEvents(df_matched,nomatch_path+f'/nonmatchlist_lhcfrun{run}_{version}.txt', arm=1)

        else:
            ##### Set first and last lb file in the run ######
            num = len(run_lb_dict[run])
            lb_list = run_lb_dict[run]
            lb_first = run_lb_dict[run][0]
            lb_last = run_lb_dict[run][num-1]
            print(f'  RunNumber{run} correspond lb{lb_first} to lb{lb_last}  ')

            ##### Read LHCf data #####
            df_a1 = ReadArm1File(arm1_path+f'LHCfArm1Run{run}.txt', arm=0)
            df_a2 = ReadArm2File(arm2_path+f'LHCfArm2Run{run}.txt', arm=0)
            df_lhcf = CombineArms(df_a1, df_a2)

            ##### Read ATLAS data #####
            # Debug with only one lumiBlock data
            # # Read ATLAS Data
            # df_atlas = ReadATLAS('../data/atlas',1483)
            # # #df_atlas.info()
            # # #print(df_atlas)

            # # ECR Offset calculation
            # ecr_offset = CalculateECRoffset(df_a2, df_atlas,arm=2,option='skipfirst')
            # df_atlas = AdjustECR(df_atlas, ecr_offset)

            # Read ATLAS data
            df_atlas = PrepareATLASdata(atlas_path, lb_list, lb_first, lb_last, df_lhcf, arm=0)

            ##### Event Matching #####
            print('Event Matching start--- ')
            df_matched, df_nomatched = EventMatching(df_lhcf, df_atlas)

            ##### Dump the matched results and the not matched result to CSV files #####
            print('\nDumping results to files...')
            DumpResult(df_matched, save_path+f'matchlist_lhcfrun{run}_{version}.txt', save_path+f'fulllist_lhcfrun{run}_{version}.txt', arm=0)
            print('\nMissing event search...')
            NotMatchingEvents(df_nomatched,nomatch_path+f'/nonmatchlist_lhcfrun{run}_{version}.txt', arm=0)
            
