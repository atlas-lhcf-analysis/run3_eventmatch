#+++++ /dump_lhcfarm1.py ++++++#
#+++++ Get data from TTree +++++#

import ROOT
import numpy as np
import os
import re

# Used in case of all root path is not common
def CollectRootFile(common_root_path, directory):
    root_file_list = []
    for fill_directory, subdirectories in directory.items():
        for subdirectory in subdirectories:
            root_path = os.path.join(common_root_path, fill_directory, subdirectory)
            root_file = [file for file in os.listdir(root_path) if file.endswith('.root')]
            root_file_list.extend([os.path.join(root_path, root_file) for root_file in root_file])
    root_file_list = sorted(root_file_list)
    return root_file_list

def GetRunNumber(root_file):
    match = re.search(r'red_run(\d{5}).root', root_file)
    if match:
        return int(match.group(1))
    else:
        print('no match')
        return None

def RunNumberList(common_root_path, subdirectories):
    root_file_list = CollectRootFile(common_root_path, subdirectories)
    run_list = [GetRunNumber(root_file) for root_file in root_file_list]
    return run_list, root_file_list

if __name__ == '__main__':
    common_root_path = '/mnt/lhcfnas3/data/JointAnalysis/Op2022/Arm1Rec_ver1_20231031/'
    subdirectories = {
        'fill8178_1': ['red'], 'fill8178_2': ['red'], 'fill8178_3': ['red'], 'fill8178_4': ['red']
    }
    save_path = './LHCfArm1_data/'
    log_path = './error_log/'

    os.makedirs(save_path, exist_ok=True)
    os.makedirs(log_path, exist_ok=True)
    run_list, root_file_list = RunNumberList(common_root_path, subdirectories)    
    log = open(log_path + 'error_log_arm1_dump.txt',mode='a', encoding = "utf-8" , newline = "\n")

    for run, filename in zip(run_list, root_file_list):
        if os.path.exists(save_path + 'LHCfArm1Run{:d}.txt'.format(run)):
            print('File LHCfArm1Run{:d}.txt already exists. Skipping...'.format(run))
            continue
        try:
            file = ROOT.TFile.Open(filename)
        except OSError as e:
            log.write(str(e)+":rec_run{:d}.root \n".format(run))
        except ValueError as e:
            log.write(str(e)+":rec_run{:d}.root \n".format(run))
        except Exception as e:
            log.write(str(e)+":rec_run{:d}.root \n".format(run))
        else:
            
            tree = file.Get("lhcfarm1")
            f = open(save_path + 'LHCfArm1Run{:d}.txt'.format(run), mode="w", encoding = "utf-8" , newline = "\n")
            nev = tree.GetEntries()
            total_data = np.zeros(8)
            print(f'\nloop start run:{run}')
            for i in range(0, nev):
                tree.GetEntry(i)

                run = tree.run
                entry = i
                time = tree.time
                time_usec = tree.time_usec
                bcid = tree.bcid
                ecr = tree.atlas_ecr
                l1id = tree.atlas_l1id
                atlas_trigger_accept = tree.atlas_trigger_accept

                add_data = np.array([run,entry,time,time_usec,bcid,ecr,l1id,atlas_trigger_accept])
                total_data = np.vstack((total_data, add_data))
                
                if total_data.shape[0]%200000 == 0:
                    np.savetxt(f,total_data[1:],delimiter=' ',fmt='%d')
                    total_data = np.zeros(8)
                    print(f'   dumping...{i}/{nev -1} ({int(i*100/nev)}%)')

            np.savetxt(f,total_data[1:],delimiter=' ',fmt='%d')
            f.close()
            print('Successfully written Run{:d}!'.format(run))
            log.close()
