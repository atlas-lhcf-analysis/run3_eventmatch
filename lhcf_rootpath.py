import ROOT
import numpy as np
import os
import re
import glob

def get_runnumber(root_file):
    match = re.search(r'red_run(\d{5}).root', root_file)
    if match:
        return int(match.group(1))
    else:
        print('no match')
        return None
    
def RunAndPathList(root_path):
    root_path_list = glob.glob(root_path)
    run_list = [get_runnumber(filename) for filename in root_path_list]
    path_dict = {run:file for run, file in zip(run_list, root_path_list)}
    path_dict = sorted(path_dict.items())
    path_dict = dict((run, path) for run, path in path_dict)
    return run_list, root_path_list, path_dict

def arm2_root_path():
    root_path = '/mnt/lhcfnas3/data/JointAnalysis/Op2022/Arm2Rec_ver2_20240116/ReducedData/*/*/*.root'
    run_list, root_path_list, path_dict = RunAndPathList(root_path)
    return path_dict

def arm1_root_path():
    root_path = '/mnt/lhcfs5/data/menjo/Op2022/Arm1Rec_ver1_20240322/*/red/*.root'
    run_list, root_path_list, path_dict = RunAndPathList(root_path)    
    return path_dict

def skip_runs():
    # skip run such as moving, emittance scan etc
    # 80267 is DAQ test
    # There is not lb coressponding to 80614
    skip_run     = [80267, 80342, 80343, 80347, 80348, 80349, 80350, 80369, 80373, 80374, 80375, 80376,
                    80432, 80466, 80467, 80468, 80469, 80470, 80521, 80531, 80532, 80533, 80534,
                    80535, 80612, 80613, 80614, 80615, 80616, 80627, 80628, 80629, 80630, 80631,
                    80632, 80633, 80634]
    not_good_run = [80433, 80522] #Not good run
    check_run    = [80509] # Pyhsics (check)
    other_run    = [80434, 80435, 80435, 80437, 80438, 80439, 80440, 80441, 80442, 80443, 80444,
                    80445, 80446, 80447, 80448, 80449, 80450, 80451, 80452, 80453, 80454, 80455,
                    80456, 80457, 80458, 80459, 80460, 80461, 80462, 80463, 80464, 80471, 80472,
                    80473] # Too many non matching events in run(excluding tmp)
    skip_run.extend(not_good_run)
    skip_run.extend(check_run)
    skip_run.extend(other_run)
    skip_run = sorted(skip_run)
    # arm2 skip run
    # 80465 is not in  /eos/experiment/lhcf/ReducedData/
    # 80465 is 'Physics' in run Table Op2022
    si_problem_run = [80397, 80506, 80507, 80549]
    only_arm1_run  = [80398, 80399, 80400, 80465, 80508, 80550, 80551]
    fc_error_run   = [80612]
    only_arm1_run.extend(si_problem_run)
    only_arm1_run.extend(fc_error_run)
    only_arm1_run = sorted(only_arm1_run)
    return skip_run, only_arm1_run
