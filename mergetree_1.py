import ROOT as R
import pandas as pd
import numpy as np
import glob
import os
import subprocess
from array import array
from lhcf_rootpath import arm1_root_path, arm2_root_path, skip_runs

class MergeTree:
    def __init__(self):
        self.match_path        = '/crhome/kinoshita.kosuke/LHCf/ATLAS_LHCf/eventmatch/eventmatch_final_22032024/'
        #self.match_path        = './eventmatch/'
        self.root_path         = '/mnt/lhcfnas3/data/kinoshita/eventmatch04032024/'
        #self.root_path         = './mergetree/'
        self.atlas_path        = '/mnt/lhcfnas3/data/JointAnalysis/Op2022/user.steinber.data22_13p6TeV.00435229.physics_MinBias.merge.AOD.r14470_p5587.2_ANALYSIS/'
        self.atlas_tree        = 'zdcTree'
        self.arm1_tree         = 'lhcfarm1'
        self.arm2_tree         = 'lhcfarm2'
        self.version           = 1

    def read_matched_table(self, arm, run):
        """
        atlas_dict:
        a dictionary key is the root file number of atlas, and values are 
        the atlas TTree "zdcTree" entry number of the root file corresponding to the key.

        arm1_entry_list:
        a list of lhcfarm1 entry number taken from matched.
        
        arm2_entry_list:
        a list of lhcfarm2 entry number taken from matched.

        """
        df = pd.read_csv(self.match_path + f'fulllist_lhcfrun{run}_{self.version}.txt', sep=' ')
        df = df.sort_values(by = ['FileNumber', 'EntryNumber'])
        df = df[df['EntryNumber'] != -1]
        atlas_dict = df.groupby('FileNumber')['EntryNumber'].apply(list).to_dict()
        if arm == 0:
            arm1_entry_list = df['Arm1EntryNumber'].tolist() 
            arm2_entry_list = df['Arm2EntryNumber'].tolist()
            return atlas_dict, arm1_entry_list, arm2_entry_list

        elif arm == 1 or arm == 2:
            entry_list = df[f'Arm{arm}EntryNumber'].tolist()
            return atlas_dict, entry_list

    def defalut_value(self, input_tree):
        """
        if entry number is -1, assign 0 to the branches of TTree "lhcfarm1" or "lhcfarm2".
        """
        val1 = array('i', [0])
        val2 = array('i', [0])
        val3 = array('i', [0])
    
        input_tree.SetBranchAddress('arm', val1)
        input_tree.SetBranchAddress('run', val2)
        input_tree.SetBranchAddress('gnumber', val3)
        return val1, val2, val3
    
    def lhcf_clone(self, lhcf_root_path, entry_list, arm, run):
        """
        The order of TTree "lhcfarm1" and "lhcfarm2" is changed to the order of matched table.
        """
        print(f'  arm{arm} tree cloning ...')
        input_file = R.TFile.Open(lhcf_root_path)
        if arm == 1:
            tree_name = self.arm1_tree

        elif arm == 2:
            tree_name = self.arm2_tree
            
        os.makedirs(f'{self.root_path}arm{arm}/', exist_ok=True)
        input_tree = input_file.Get(tree_name)
        output_file = R.TFile(f'{self.root_path}arm{arm}/arm{arm}_run{run}_{self.version}.root', 'recreate')
        output_tree = input_tree.CloneTree(0)
        val1, val2, val3 = self.defalut_value(input_tree)
        for i in entry_list:
            if i >= 0:
                input_tree.GetEntry(i) #25
                output_tree.Fill()
            elif i <0:
                val1[0] = 0
                val2[0] = 0
                val3[0] = 0
                output_tree.Fill()
        output_file.Write()
        output_file.Close()
        input_file.Close()
        
    def atlas_clone(self, entry_dict, run, file_number_list):
        """
        Creating TTree "zdcTree" corresponding to lhcf run and splited by atlas root file number.   
        """
        print('  atlas tree cloning ...')
        os.makedirs(f'{self.root_path}atlas/', exist_ok=True)
        for file_number in file_number_list:
            input_file = R.TFile.Open(self.atlas_path + f'user.steinber.34020985.ANALYSIS._{file_number:06}.root')
            input_tree = input_file.Get(self.atlas_tree)
            output_file = R.TFile(f'{self.root_path}atlas/atlas_run{run}_file{file_number:06}.root', 'recreate')
            output_tree = input_tree.CloneTree(0)            
            for i in entry_dict[file_number]:
                input_tree.GetEntry(i)
                output_tree.Fill()
            output_file.Write()
            output_file.Close()
            input_file.Close()
        
    def atlas_file_list(self, run):
        """
        Take the list including path of root file created by this class method "atlas_clone".
        """
        file_list = glob.glob(f'{self.root_path}atlas/atlas_run{run}_file*.root')
        file_list = sorted(file_list)
        if not file_list:
            raise FileNotFoundError('No files found for the specified option and run') 
        return file_list

    def combine_atlas_file(self, run):
        """
        Combine files divided by the atlas root file number into one file using hadd.
        """
        input_files = self.atlas_file_list(run)
        subprocess.run(["hadd", f'{self.root_path}atlas/atlas_run{run}_{self.version}.root'] + input_files)
    
    def file_remove(self, run):
        """
        Remove unnecessary root file
        """
        for filepath in glob.glob(f'{self.root_path}atlas/atlas_run{run}_file*'):          
            if os.path.isfile(filepath):
                os.remove(filepath)

if __name__ == '__main__':
    # see lhcf_rootpath.py 
    skip_run, only_arm1 = skip_runs()
    arm1_path_dict = arm1_root_path()
    arm2_path_dict = arm2_root_path()
    
    merge_tree = MergeTree()
    for run in runs:
        if run in skip_run:
            continue
        
        #clone lhcf tree 
        elif run in only_arm1:
            atlas_entry_dict, arm1_entry_list = merge_tree.read_matched_table(1, run)
            print(f'merge tree run{run}')
            merge_tree.lhcf_clone(arm1_path_dict[run], arm1_entry_list, 1, run)

        else:
            atlas_entry_dict, arm1_entry_list, arm2_entry_list = merge_tree.read_matched_table(0, run)
            print(f'merge tree run{run}')
            merge_tree.lhcf_clone(arm1_path_dict[run], arm1_entry_list, 1, run)
            merge_tree.lhcf_clone(arm2_path_dict[run], arm2_entry_list, 2, run)

        #clone atlas tree
        merge_tree.atlas_clone(atlas_entry_dict, run, atlas_entry_dict.keys())
            
        #combine and remove atlas root file
        merge_tree.combine_atlas_file(run)
        merge_tree.file_remove(run)

