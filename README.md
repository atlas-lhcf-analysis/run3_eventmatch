# run3_eventmatch

This project is for an event matching between LHCf and ATLAS data obtained in 2022. This requires python3 with pandas, pyroot packages. The main developer was K. Kosuke. 

## Strategy

- Dump the minimum valuables from each ATLAS and LHCf (arm1 and arm2) file. 
- Event matching is performed using ATLAS Extended L1ID recorded in both ATLAS and LHCf data. 
   - ATLAS LumiBlock (LB) corresponding to a LHCf run can be found from the start/end time of them.
   - The result is dumped to a text file
- Sort the events in both ATLAS and LHCf files according to the event-match table.  

## How to use the codes

#### dump_lhcfarm1.py and dump_lhcfarm2.py 
- These codes dump valuables event-by-event from a LHCf run to a text file.
- Run as 
```
python3 dump_arm1.py (or dump_arm2.py)
```
- Input : LHCf-Arm1 (or Arm2) "Reduction" files. Each file contain data of a single run. The directory path where these files are is specified as "common_root_path" in the code. 
- Output: Save to the "LHCfArm1_data" or "LHCfArm2_data" directory. Each output file contains the following valuables for each event with CSV format. 
```
LHCf_run_number entry_number  time_stamp(sec) time_stamp(usec) Bunch_ID ECR L1ID L1A_flag
```
- Note: Bunch_ID and ECR have constant offsets with ATLAS definition

#### dump_atlas.py 
- This code dump valuables for a LB in a ATLAS TTree file. 
- This code needs text files prepared by Peter, which contain lists of entry ID for given LB and file ID. The directory path of these files are specified as ```lb_path``` in the code.
- The TTree files are specified by the directory path ```file_path``` and the filename format ```filename```
- Run as 
```
python3 dump_atlas.py 
```
- Output: Save to the "LHCfArm2_data" directory. Each output file contains the following valuables for each event with CSV format.
```
entry_number file_number atlas_run_number LB Bunch_ID time_stamp(sec) time_stamp(nsec) ExtL1ID LHCf_Trg_Flag
```

#### eventmatch_1.py
- Make a table of matched events 
   1. Firstly create a correspondence table between LHCf run and ATLAS LB using start and end time of them. (MakeRunlbTable)
   2. Read both Arm1 and Arm2 event tables of a run, and combine them to a single event table (CombineArms).
   3. Read ATLAS event tables (files and LBs) for the LHCf run, and combine to a single event table sorted by time (PrepareATLASdata). In this function, the offset of ECR counts in LHCf is checked and corrected.
   4. Event match between prepared LHCf and ATLAS tables using ECR and L1ID (pandas merge with left (to LHCf table) option) (EventMatching)
   5. Dump the results to files. 
- User must change the version number '''version''' and the directory path of output files '''save_path''' 
- There are three output files:
   - matchlist_*.txt :  only LHCf run and entry Id, and ATLAS file Id and entry Id
   - fulllist_*.txt : all valuables of matched tables including L1ID, Bunch ID, time stamp. It can be used for checks and debug.
   - nonmatchlist_*.txt : only events with no ATLAS data are dumped to this file. 

#### mergetree_1.py
- Sort each original TTree with the order of event-match table. All branches of TTree are saved to a new sorted TTree with the same format as the original TTree.
- Inputs are, directory path of matched tables ('''self.match_path''') and original LHCf TTree ('''root_path''' of lhcf_rootpath.py) and original ATLAS TTree ('''self.root_path'''). 
- The version number '''self.version''' must be specified. 
- The output TTree are sorted by the order of ATLAS File ID and Entry ID. (not time) 

#### Comments
- All these codes run as a single process for all data. So each step takes 1-3 days to be completed.

## How to analyze the data
- Read all of sorted LHCf Arm1, Arm2 and ATLAS TTree simultaneously.
- Sample code : /eos/experiment/atlas-lhcf/Run3Ntuples/MatchingResult17032024/tree/sample_jointanalysis.py

## Event-match version

- Version 2024-03-17
   - Original TTree files
      - ATLAS : /eos/experiment/atlas-lhcf/Run3Ntuples/user.steinber.data22_13p6TeV.00435229.physics_MinBias.merge.AOD.r14470_p5587.2_ANALYSIS
      - LHCf Arm1 :  Nagoya's server:/mnt/lhcfs5/data/menjo/Op2022/Arm1Rec_ver1_20240322 (processed in 2024 April) Git Hash(bc92fcc)
      - LHCf Arm2 :  Nagoya's server:/mnt/lhcfs5/data/menjo/Op2022/Arm2Rec_ver1_20231124
   - Data path : /eos/experiment/atlas-lhcf/Run3Ntuples/MatchingResult17032024/
   - Comments 
      - AFP valuables are not finalized yet in ATLAS data.
      - Calibration parameters in LHCf are still preliminary.



