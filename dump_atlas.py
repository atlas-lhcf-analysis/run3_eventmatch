#===== ~/work/Mariadb/src/foge1.py =====#
import pandas as pd
import numpy as np
import os
import re
import ROOT
import glob

def get_number(lb_file):
    match = re.search(r'lb(\d{4})_(\d{3})\.txt', lb_file)
    if match:
        return int(match.group(1)), int(match.group(2))
    else:
        print('no match')
        return None
    
#--- get file name ---#
lb_path = "/mnt/lhcfnas3/data/JointAnalysis/Op2022/Run3LBFiles_2/Run3LBFiles_2/"
file_path = "/mnt/lhcfnas3/data/JointAnalysis/Op2022/user.steinber.data22_13p6TeV.00435229.physics_MinBias.merge.AOD.r14470_p5587.2_ANALYSIS/"
filename = 'user.steinber.34020985.ANALYSIS._{:06}.root'
save_path = './ATLAS_data/'
log_path = './error_log/'

os.makedirs(save_path, exist_ok=True)
os.makedirs(log_path, exist_ok=True)
log = open(log_path+"error_log_atlas_dump.txt", mode="w", encoding = "utf-8", newline = "\n")
lb_files = sorted(glob.glob(lb_path+'*'))
#--- get lumiblock and root file number---#
print('loop start')
for j,lb_file in enumerate(lb_files):
    try:
        EntryNumbers = np.loadtxt(lb_file,dtype=int)[:,1]
        lb,FileNumber = get_number(lb_file)
        if lb != 1505 and FileNumber == 121:
            print(f'{lb}skip')
            continue
        #--- open root file and make text file---#
        file = ROOT.TFile.Open(file_path + filename.format(FileNumber))
        tree = file.Get("zdcTree")

        #--- event roop ---#
        Header = ["entry_num","file_num","run_num","lb","bcid","timeStamp","timeStampNSOffset","extendedlevel1ID","HLT_noalg_L1LHCF"]
        df = pd.DataFrame(columns = Header)
        total_data = np.zeros(9)
        with open(save_path+f'lumiblock_{lb:04}_rootnumber_{FileNumber:03}.txt', mode='w', encoding='utf=8') as f:
            for i,EntryNumber in enumerate(EntryNumbers,0):
                tree.GetEntry(EntryNumber)
                entry_num         = EntryNumber
                file_num          = FileNumber
                run_num           = tree.runNumber
                lb                = tree.lumiBlock
                bcid              = tree.bcid
                timeStamp         = tree.timeStamp
                timeStampNSOffset = tree.timeStampNSOffset
                extendedlevel1ID  = tree.extendedLevel1ID
                HLT_noalg_L1LHCF  = tree.HLT_noalg_L1LHCF
                
                event_data = [entry_num,file_num,run_num,lb,bcid,timeStamp,timeStampNSOffset,extendedlevel1ID, HLT_noalg_L1LHCF]
                total_data = np.vstack((total_data,np.array(event_data, dtype = 'int64')))

                if total_data.shape[0]%30000 == 0:
                    np.savetxt(f,total_data[1:],delimiter=' ',fmt='%d')
                    total_data = np.zeros(9)
            print(total_data[:1])
            np.savetxt(f,total_data[1:],delimiter=' ',fmt='%d')
            print(f'{lb_file}dumping...{j}/{len(lb_files)}')
            
    except OSError as e:
        log.write(f'Not found root file {filename}\n')
        print(f'Not found root file {filename}')

    except Exception as e:
        log.write(f'{e} in {lb_file} \n')
        print(f'{e} in LB{lb}_{FileNumber:03}')
        
print('all complete')
